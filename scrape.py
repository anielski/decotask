#!/usr/bin/env python

import urllib2
from bs4 import BeautifulSoup


url = 'http://www.grahamandgreen.co.uk/sofas-armchairs?limit=all'


def get_products(url):
	# scrape html content of the website
	response = urllib2.urlopen(url)
	html = response.read()
	soup = BeautifulSoup(html)
	# get desired section and make a list of products subsections
	items = soup.find(id='list-products').find_all('li')
	result = []
	for item in items:
		# extract inner html of name and price
		# name is taken as is
		name = item.find('a', 'name').text
		price = item.find('div', 'pricing').text
		# price format can be described as: "\\xa3\d+.\d\d" {" - \\xa3\d+.\d\d"}
		# that is "P1234.56" OR "P1234.56 - P2345.67" , where P is the pound symbol
		# pound symbol, space and comma are removed
		price = "".join([e for e in price if e not in u", \xa3"]).split('-')
		# and price can be converted to float
		# price = [float(e) for e in price]
		result.append((name, price))
	return result


def main(url, output):
	products = get_products(url)
	# write results to a file in csv format
	with open(output, 'w') as f:
		for n, p in products:
			f.write(n + ',' + '-'.join(p) + '\n')


if __name__ == '__main__':
	output = "products.csv"
	main(url, output)
