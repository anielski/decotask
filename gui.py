#!/usr/bin/env python

import scrape
from Tkinter import *
import ttk
import tkMessageBox

root = Tk()
root.title("Scraper")

ext = ".csv"


def is_bad_char(e): return e in ".+=!@#$%^&*,<>;:'\"{}\\|/~`"


def save_info(*args):
	try:
		outfname = out.get()
		if len(outfname) == 0:
			raise ValueError("Empty filename")
		badchars = filter(is_bad_char, outfname)
		if badchars:
			raise ValueError("Unsupported characters in filename: " + badchars)
		outfname += ext
		scrape.main(scrape.url, outfname)
		tkMessageBox.showinfo("Success", "Successfully saved products info to " + outfname)
	except ValueError as e:
		tkMessageBox.showerror("Error", "Error occurred while saving products info to " + outfname + "\n" + e.message)


w = Label(root, text="Enter output file name:")
out = StringVar()
out_entry = ttk.Entry(root, textvariable=out)
b = ttk.Button(root, text="Save info", command=save_info)

w.pack()
out_entry.pack()
b.pack()

root.bind('<Return>', save_info)

root.mainloop()
